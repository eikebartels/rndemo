//
//  AddressModel.swift
//  Taxfix
//
//  Created by Eike Bartels on 22/02/2017.
//  Copyright © 2017 Eike Bartels. All rights reserved.
//

import Foundation
import ObjectMapper

struct AddressModel {
    let id:String
    let formattedAddress:String
    let latitude:Double
    let longitude:Double
}

extension AddressModel:ImmutableMappable {
    
    init(map: Map) throws {
        id                  = try map.value("placeID")
        latitude            = try map.value("lat")
        longitude           = try map.value("lng")
        formattedAddress    = try map.value("formattedAddress")
    }
}

extension AddressModel: Equatable {}

// MARK: Equatable

func == (lhs: AddressModel, rhs: AddressModel) -> Bool {
    return lhs.id == rhs.id
}

extension AddressModel:CustomStringConvertible {
    var description: String {
        return "\(self.formattedAddress)"
    }
}
