//
//  TaxfixReactModule.swift
//  Taxfix
//
//  Created by Eike Bartels on 21/02/2017.
//  Copyright © 2017 Eike Bartels. All rights reserved.
//

import Foundation
import React

class ReactModuleManager: NSObject {
    internal var bridge: RCTBridge?
    static let shared = ReactModuleManager()
    
    internal func createBridgeIfNeeded() -> RCTBridge {
        if bridge == nil {
            bridge = RCTBridge.init(delegate: self, launchOptions: nil)
        }
        return bridge!
    }
    
    internal func viewForModule(_ moduleName: String, initialProperties: [String : Any]?) -> RCTRootView {
        let viewBridge = createBridgeIfNeeded()
        let rootView: RCTRootView = RCTRootView(bridge: viewBridge, moduleName: moduleName, initialProperties: initialProperties)
        return rootView
    }
}

extension ReactModuleManager: RCTBridgeDelegate {
    internal func sourceURL(for bridge: RCTBridge!) -> URL! {
        return URL(string: "http://localhost:8081/index.ios.bundle?platform=ios")
    }
}
