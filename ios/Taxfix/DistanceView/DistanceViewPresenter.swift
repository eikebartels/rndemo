//
//  DistanceViewPresenter.swift
//  Taxfix
//
//  Created by Eike Bartels on 22/02/2017.
//  Copyright © 2017 Eike Bartels. All rights reserved.
//

import Foundation



protocol DistanceView:NSObjectProtocol {
    var workAddress:String? {get set}
    var homeAddress:String? {get set}
    var distance:String? {get set}
    
    func pushAddressPicker() -> AddressPicker
    func popAddressPicker()
    
    func startLoading()
    func stopLoading()
}

class DistanceViewPresenter {
    enum AddressRequestType {
        case home
        case work
    }
    
    fileprivate var homeAddressModel:AddressModel? {
        didSet {
            if let address = homeAddressModel {
                view?.homeAddress = address.formattedAddress
            }
        }
    }
    fileprivate var workAddressModel:AddressModel?{
        didSet {
            if let address = workAddressModel {
                view?.workAddress = address.formattedAddress
            }
        }
    }
    
    fileprivate weak var view:DistanceView?
    fileprivate var addressPicker:AddressPicker?
    fileprivate var requestedAddress:AddressRequestType?
    private var distanceCalculatorClass:DistanceCalculator.Type!
    
    // MARK: -
    // MARK: Functions got called from view
    init(distanceCalculatorClass:DistanceCalculator.Type) {
        self.distanceCalculatorClass = distanceCalculatorClass
    }
    
    internal func attachView(view:DistanceView) {
        self.view = view
    }
    
    internal func detachView() {
        view = nil
    }
    
    internal func requestHomeAddress() {
        self.requestedAddress = .home
        addressPicker = view?.pushAddressPicker()
        addressPicker?.delegate = self
    }
    
    internal func requestWorkAddress() {
        self.requestedAddress = .work
        addressPicker = view?.pushAddressPicker()
        addressPicker?.delegate = self
    }
    
    private var calculator:DistanceCalculator?
    
    internal func tryCalculateDistance() {
        guard let homeAddress = homeAddressModel,
            let workAddress = workAddressModel else {
            return
        }
        view?.startLoading()
        calculator = distanceCalculatorClass.init(source: homeAddress, destination: workAddress)
        calculator?.calculate(completionHandler: { [weak self] (distance, error) in
            guard let strongSelf = self else { return }
            print("Distance \(distance) \(error)")
            strongSelf.view?.distance = "\(Int(distance / 1000)) km"
            strongSelf.view?.stopLoading()
        })
    }
}

extension DistanceViewPresenter:AddressPickerDelegate {
    func didSelectAddress(address: AddressModel) {
        if let requestedAddress = self.requestedAddress {
            switch requestedAddress {
                case .home: homeAddressModel = address; break
                case .work: workAddressModel = address; break
            }
            
            view?.popAddressPicker()
        }
        tryCalculateDistance()
        self.addressPicker = nil
    }
}
