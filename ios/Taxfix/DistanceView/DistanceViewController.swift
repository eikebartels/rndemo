//
//  DistanceViewController.swift
//  Taxfix
//
//  Created by Eike Bartels on 19/02/2017.
//  Copyright © 2017 Eike Bartels. All rights reserved.
//

import UIKit

class DistanceViewController: UIViewController {
    private var presenter = DistanceViewPresenter(distanceCalculatorClass: NativeDistanceCalculator.self)
    
    @IBOutlet fileprivate weak var homeAddressLabel: UILabel!
    @IBOutlet fileprivate weak var workAddressLabel: UILabel!
    @IBOutlet fileprivate weak var distanceLabel: UILabel!
    @IBOutlet fileprivate weak var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
    }
    
    @IBAction func selectHomeAddress(sender:UIButton) {
        presenter.requestHomeAddress()
    }
    
    @IBAction func selectWorkAddress(sender:UIButton) {
        presenter.requestWorkAddress()
    }
}

extension DistanceViewController:DistanceView {
    func pushAddressPicker() -> AddressPicker {
        let picker = AddressPickerViewController()
        show(picker, sender: self)
        return picker
    }

    func popAddressPicker() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    internal var homeAddress: String? {
        get{
            return homeAddressLabel.text
        }
        set {
            homeAddressLabel.text = newValue
        }
    }

    internal var workAddress: String? {
        get{
            return workAddressLabel.text
        }
        set {
            workAddressLabel.text = newValue
        }
    }

    var distance: String? {
        get{
            return distanceLabel.text
        }
        set {
            distanceLabel.text = newValue
        }
    }
    
    func startLoading() {
        indicator.startAnimating()
    }
    
    func stopLoading() {
        indicator.stopAnimating()
    }
}
