//
//  DistanceCalculator.swift
//  Taxfix
//
//  Created by Eike Bartels on 22/02/2017.
//  Copyright © 2017 Eike Bartels. All rights reserved.
//

import Foundation


public typealias DistanceCalculatorHandler = (Double, Error?) -> Void

protocol DistanceCalculator:class {
    init(source:AddressModel, destination:AddressModel)
    func calculate(completionHandler: @escaping DistanceCalculatorHandler)
}
