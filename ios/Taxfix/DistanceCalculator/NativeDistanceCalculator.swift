//
//  AppleDistanceCalculator.swift
//  Taxfix
//
//  Created by Eike Bartels on 22/02/2017.
//  Copyright © 2017 Eike Bartels. All rights reserved.
//

import Foundation
import MapKit

class NativeDistanceCalculator:DistanceCalculator {
    internal let source:AddressModel
    internal let destination:AddressModel
    
    private lazy var request: MKDirectionsRequest = MKDirectionsRequest()
    
    required init(source:AddressModel, destination:AddressModel) {
        self.source = source
        self.destination = destination
    }
    
    private func mapItem(address:AddressModel) -> MKMapItem {
        let coordinates = CLLocationCoordinate2D(latitude: address.latitude, longitude: address.longitude)
        let placemark = MKPlacemark(coordinate:coordinates )
        return MKMapItem(placemark: placemark)
    }
    
    private func distance(route:MKRoute) -> Double {
        return route.distance
    }
    
    func calculate(completionHandler: @escaping DistanceCalculatorHandler) {
        let source = mapItem(address: self.source)
        let destination = mapItem(address: self.destination)
        
        request.source = source
        request.destination = destination
        request.requestsAlternateRoutes = true
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        directions.calculate(completionHandler: { [weak self](response, error) in
            guard let strongSelf = self else { return }
            guard let response = response,
                let route = response.routes.first else {
                    
                    completionHandler(-1, error)
                    return
            }
            
            completionHandler(strongSelf.distance(route: route), error)
            
        })
    }
}
