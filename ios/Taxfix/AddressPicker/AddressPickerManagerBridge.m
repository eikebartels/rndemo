//
//  AddressPickerBridge.m
//  Taxfix
//
//  Created by Eike Bartels on 19/02/2017.
//  Copyright © 2017 Eike Bartels. All rights reserved.
//

#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(AddressPickerManager, NSObject)

RCT_EXTERN_METHOD(didSelectAddress:(nonnull NSNumber *)reactTag address:(NSDictionary *)address)


@end
