//
//  AddressPickerManager.swift.swift
//  Taxfix
//
//  Created by Eike Bartels on 21/02/2017.
//  Copyright © 2017 Eike Bartels. All rights reserved.
//

import Foundation
import React

@objc(AddressPickerManager)
class AddressPickerManager: RCTEventEmitter {
    
    override func supportedEvents() -> [String]! {
        return ["AddressPickerManagerEvent"]
    }
    
    @objc func didSelectAddress(_ reactTag: NSNumber, address:NSDictionary) {
        DispatchQueue.main.async {
            if let view = self.bridge.uiManager.view(forReactTag: reactTag) {
                if let vc = view.reactViewController() as? AddressPickerViewController {
                    vc.didSelectAddress(address: address)
                }
            }
        }
    }
}
