//
//  AddressPickerViewController.swift
//  Taxfix
//
//  Created by Eike Bartels on 19/02/2017.
//  Copyright © 2017 Eike Bartels. All rights reserved.
//

import UIKit
import React
import SnapKit
import ObjectMapper

protocol AddressPicker {
    var delegate:AddressPickerDelegate? {get set}
}

protocol AddressPickerDelegate:class {
    func didSelectAddress(address:AddressModel)
}

class AddressPickerViewController: UIViewController, AddressPicker {

    weak var delegate:AddressPickerDelegate?
    
    var addressView: RCTRootView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = []
        
        addressView = ReactModuleManager.shared.viewForModule("AddressPicker", initialProperties:nil)
        view.addSubview(addressView)
        
        addressView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
    
    //MARK: Functions called from AddressPickerManager
    
    func didSelectAddress(address:NSDictionary) {
        guard let address = address as? [String : Any] else {
            print("Address not valide. to something")
            return
        }
        
        do {
            let addressModel = try Mapper<AddressModel>().map(JSONObject: address)
            delegate?.didSelectAddress(address: addressModel)
        } catch {
            print("Faild to parse Address. Do some handeling")
        }
    }
}
