# Sample App  #

This repository contains an react native + swift example 

### What is this repository for? ###

* Demonstration
* Version: 0.0.1

### What is inside ###
* React native component to find and select an address
* native component to calculate a distance between 2 addresses 
* native code is using the MVP pattern for better testability

### Pods in use ###

* SnapKit for handling layout constraints 
* ObjectMapper for mapping react native response to native address model 


### How do I get set up? ###

* Clone repo
* run npm install 
* run npm start 
* open io/ios/Taxfix.xcworkspace
* run Xcode project 

If you get an error 

~~~~
#include <jschelpers/JSCWrapper.h>
~~~~
replace with 

~~~~
#include "JSCWrapper.h"
~~~~