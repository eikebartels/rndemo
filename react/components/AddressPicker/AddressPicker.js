// @flow

import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TextInput,
  ListView,
  TouchableHighlight,
  Text,
  NativeModules,
} from 'react-native';

const { AddressPickerManager } = NativeModules;

import Service from './AddressService'

class AddressPicker extends React.Component {

    state:any
    service:Service

    constructor(props) {
      super(props);
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.state = {
          isLoading: true,
          placeholder: 'Tip In Address',
          dataSource: new ListView.DataSource({
              rowHasChanged: (row1, row2) => row1 !== row2,
          }),
          listItems: ds.cloneWithRows([]),
          address:null
      };

      this.service = new Service()
      this.service.onDataReceived = this._onDataReceived

    }

    _onDataReceived = (data) => {
        var ds = this.state.dataSource.cloneWithRows(data);
         this.setState({
             listItems: ds
         });
    }

    _updateText = (text) => {
        this.service.fetch(text)
    }

    _onPressRow(rowID:any, rowData:any) {
        AddressPickerManager.didSelectAddress(this.props.rootTag, rowData);
    }
    _renderRow(rowData: any, sectionID: number, rowID: number) {
        console.log('render row ID:' + rowID + " sectionID: " + sectionID);
        return (
            <TouchableHighlight activeOpacity={0.6}
                                underlayColor={'gray'}
                                onPress={() => {
                                    this._onPressRow(rowID, rowData)
                                 }}>
            <View style={styles.row}>
                <Text style={styles.text}>
                    {`${rowData.formattedAddress}`}
                </Text>
            </View>
            </TouchableHighlight>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                 <TextInput
                     autoCorrect={false}
                     placeholder= {this.state.placeholder}
                     style={styles.textInput}
                     onChangeText={(address) => {
                         this.setState({address})
                         this._updateText(address);
                      }}
                     value={this.state.address}
                     autoFocus={true}
                  />
                  <ListView
                    dataSource={this.state.listItems}
                    renderRow = {this._renderRow.bind(this)}
                    renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                  />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    textInput: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 1,
        padding:10
    },
    separator: {
       flex: 1,
       height: StyleSheet.hairlineWidth,
       backgroundColor: '#8E8E8E',
    },
    row: {
      flex: 1,
      padding: 12,
      flexDirection: 'row',
      alignItems: 'center',
    },
    text: {
      marginLeft: 0,
      fontSize: 16,
    }
});

// Module name
AppRegistry.registerComponent('AddressPicker', () => AddressPicker);
