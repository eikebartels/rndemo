// @flow
export default class AddressService {
    isLoading:bool = false;
    request:XMLHttpRequest;
    onDataReceived:(results:[any]) => any;

    constructor() {
        this.request = new XMLHttpRequest();
        this.request.onabort = (e) => {
            this.isLoading = false
        }
        this.request.onreadystatechange = (e) => {
          if (this.request.readyState !== 4) {
            return;
          }

          if (this.request.status === 200) {
              var jsonResponse = JSON.parse(this.request.responseText);
            //   console.log('success', jsonResponse);
              this.parseResponse(jsonResponse)
          } else {
            console.warn('error');
          }
        };
    }
    _convertData(rowData:any){
            var object = {}
            object.formattedAddress = rowData.formatted_address
            object.lat = rowData.geometry.location.lat
            object.lng = rowData.geometry.location.lng
            console.log(object)
            return object
    }
    parseResponse(jsonResponse:any) {
        var results = jsonResponse.results
        var addresses = []
        for(var element of results) {
            var object = {}
            object.formattedAddress = element.formatted_address
            object.lat = element.geometry.location.lat
            object.lng = element.geometry.location.lng
            object.placeID = element.place_id
            addresses.push(object)
        }

        this.onDataReceived(addresses)
    }

    fetch(text:string) {
        if (text.length < 4) {
            return;
        }
        this.isLoading = true
        var query = text.split(' ').join('+');
        console.log("start fetching geo infos for: " + query);
        console.log('https://maps.googleapis.com/maps/api/geocode/json?address='+ query);
        this.request.abort()
        this.request.open('GET', 'https://maps.googleapis.com/maps/api/geocode/json?address='+ query);
        this.request.send();
    }
}
